import Header from './components/UI/Header'

const RootLayout = ({ children }) => {
  return (
    <>
      <Header/>
      <main style={{ backgroundColor: '#46465c' }}>{children}</main>
    </>
  )
}

export default RootLayout;
