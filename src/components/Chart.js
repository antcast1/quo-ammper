import React from "react";
import Highcharts from "highcharts";
import ReactHighChart from "highcharts-react-official";
import HighchartsMore from "highcharts/highcharts-more";

import bellCurve from "highcharts/modules/histogram-bellcurve";
import exporting from "highcharts/modules/exporting";
import exportData from "highcharts/modules/export-data";

export default function Chart({ options = {} }) {

  HighchartsMore(Highcharts);

  exporting(Highcharts);
  exportData(Highcharts);
  bellCurve(Highcharts);


  return (
    <ReactHighChart highcharts={Highcharts} options={options} />
  );
}