import { Typography } from '@mui/material'
import React from 'react'

const Header = () => {
    return (
        <>
            <div style={{ display: 'flex', backgroundColor: '#f0f0f0', width: '100%', height: '7vh', alignItems: 'center' }}>
                <div style={{ marginLeft: 12 }}>
                    <Typography fontWeight={700} fontSize={20}>Quo Ammper</Typography>
                </div>
            </div>
        </>
    )
}

export default Header