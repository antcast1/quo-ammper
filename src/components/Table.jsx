// Components
import { DataGrid } from '@mui/x-data-grid';

// Style
import { Card, Typography } from '@mui/material';
import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';

export async function getStaticPaths() {
  // Generate a list of possible values for the "slug" dynamic segment
  const paths = [
    { params: { slug: 'value1' } },
    { params: { slug: 'value2' } },
    // Add more paths as needed
  ];

  // Return the paths object
  return {
    paths,
    fallback: false, // or true if you want to use fallback behavior
  };
}

const Table = ({ rows, account, isLoading, isError }) => {

  const columns = [
    { field: 'amount', headerName: 'Amount', flex: 1 },
    { field: 'category', headerName: 'Category', flex: 1 },
    { field: 'description', headerName: 'Description', flex: 1 },
    { field: 'created_at', headerName: 'Date', flex: 1 },
    { field: 'status', headerName: 'Status', flex: 1 },
    { field: 'balance', headerName: 'Balance', flex: 1 },
  ]

  return (
    <Box>
      {
        isLoading && !isError ? (
          <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '90vh' }}>
            <CircularProgress />
          </Box>
        ) : !isLoading && !isError ? (
          <Box sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignContent: 'center', height: '90vh', width: '90vw' }}>
            <Card sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignContent: 'center', height: '90vh', width: '96%', borderRadius: '16px', padding: 4, backgroundColor: '#f0f0f0' }}>
              <div>
                <Typography fontSize={"21px"}>
                  Estado de Cuenta de <span style={{ fontWeight: 700 }}> {account?.display_name ?? ""}</span>
                </Typography>
              </div>
              <div style={{ height: '95%', backgroundColor: 'white' }}>
                <DataGrid
                  rows={rows}
                  columns={columns}
                  initialState={{
                    pagination: {
                      paginationModel: { page: 0, pageSize: 10 },
                    },
                  }}
                  pageSizeOptions={[10, 50, 100]}
                />
              </div>
            </Card>
          </Box>
        ) : (<></>)
      }
    </Box>
  )
}

export default Table