import { createSlice } from "@reduxjs/toolkit";
import { generateTransactions } from "../actions/getTransactions";

const initialState = {
    transactions: [],
    isLoading: false,
    isError: false
}

export const transactionsSlice = createSlice({
    name: 'transactionsUser',
    initialState,
    reducers: {
        clearState: (state) => {
            state.transactions = [];
            state.isError = false;
        }
    },
    extraReducers: (builder) => {
        builder

        .addCase(generateTransactions.pending, (state, action) => {
            state.isLoading = true;
            state.isError = false;
        })
        .addCase(generateTransactions.fulfilled, (state, action) => {
            state.transactions = action.payload.results;
            state.isLoading = false;
        })
        .addCase(generateTransactions.rejected, (state, action) => {
            state.isError = true;
            state.transactions = [];
            state.isLoading = false;
        })
    }
})

export const { clearState } = transactionsSlice.actions;
export default transactionsSlice.reducer;
export const transactionsSelector = (state) => state.transactionsSlice;