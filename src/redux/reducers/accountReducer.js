import { createSlice } from "@reduxjs/toolkit";
import { generateAccount } from "../actions/getAccountActions";

const initialState = {
    account: null,
    isLoading: false,
    isError: false
}

export const accountSlice = createSlice({
    name: 'accounts',
    initialState,
    reducers: {
        clearState: (state) => {
            state.account = [];
            state.isError = false;
        }
    },
    extraReducers: (builder) => {
        builder

        .addCase(generateAccount.pending, (state, action) => {
            state.isLoading = true;
            state.isError = false;
        })
        .addCase(generateAccount.fulfilled, (state, action) => {
            state.account = action.payload.results.length > 0 ? action.payload.results[0] : null;
            state.isLoading = false;
        })
        .addCase(generateAccount.rejected, (state, action) => {
            state.isError = true;
            state.account = null;
            state.isLoading = false;
        })
    }
})

export const { clearState } = accountSlice.actions;
export default accountSlice.reducer;
export const accountSelector = (state) => state.accountSlice;