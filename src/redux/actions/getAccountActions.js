import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const BASE_URL = `https://sandbox.belvo.com/`;

export const generateAccount = createAsyncThunk(
  "account/generateAccount",
  async (thunkAPI) => {
    try {
      const options = {
        method: "GET",
        url: `${BASE_URL}api/owners/?page=1&link=55cf42f9-6b4e-46c5-b246-a83598a8695d`,
        params: { page_size: "100" },
        headers: {
          accept: "application/json",
          authorization:
            "Basic YjY0YTQ0NzAtNGUzOC00MTQ5LTg1MTktYTYzNjMyNTMzMmUyOnFEWC1yRWthdGZAM0JNRmxMeGliM0hXaUJ1enBuNm5vQ3dwNkxKRnQwbEhhWjNjQ3RKVEVQOU5ObFNUMHZsdjQ=",
        },
      };
      const response = await axios.request(options);
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);
