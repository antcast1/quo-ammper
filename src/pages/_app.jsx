import Head from "next/head";
import { Provider } from "react-redux";
import { store } from '../store';
import RootLayout from "../layout";

export default function MyApp({ Component, pageProps }) {
    return (<>
        <Head>
            <title>Quo Ammper</title>
            <link rel="icon" href="/favicon.ico" sizes="any" />
        </Head>
        <Provider store={store}>
            <RootLayout>
                <Component {...pageProps} />
            </RootLayout>
        </Provider>
    </>)
}