// Components
import Table from "../components/Table";
import Chart from "@/components/Chart";

// Redux
import { transactionsSelector } from '../redux/reducers/transactionsReducer';
import { accountSelector } from '../redux/reducers/accountReducer';
import { generateTransactions } from '../redux/actions/getTransactions';
import { generateAccount } from '../redux/actions/getAccountActions';
import { useDispatch, useSelector } from 'react-redux';

// React
import { useEffect, useState } from 'react';

// Styles
import styles from "../page.module.css";
import { Card, Grid, Box } from "@mui/material";
import CircularProgress from '@mui/material/CircularProgress';

//MOMENT
import moment from "moment"

export default function Home() {
  // TABLE
  const dispatch = useDispatch()
  const { transactions, isLoading, isError } = useSelector(transactionsSelector)
  const { account } = useSelector(accountSelector)

  // CHART
  const [options, setOptions] = useState(null)
  const [options2, setOptions2] = useState(null)
  // const [options3, setOptions3] = useState(null)
  // const [options4, setOptions4] = useState(null)
  // const [options5, setOptions5] = useState(null)

  // TABLE
  const postTransactions = () => {
    let body = {
      date_from: "2023-06-23",
      date_t: "2023-09-23",
    }
    dispatch(generateTransactions(body));
  }
  const getAccount = () => {
    let body = {}
    dispatch(generateAccount(body));
  }
  useEffect(() => {
    postTransactions();
    getAccount();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  //Dispertion
  useEffect(() => {
    setOptions({
      chart: {
        type: 'bubble',
        plotBorderWidth: 1,
        zoomType: 'xy'
      },
      legend: {
          enabled: false
        },
        title: {
          text: 'Dispertion',
          align: 'left'
        },
        subtitle: {
          text: 'Transactions',
          align: 'left'
        },
        xAxis: {
          gridLineWidth: 1,
          title: {
            text: 'Date'
          },
          type: 'datetime',
          tickInterval: 24 * 36e5,
          labels: {
            formatter: function () {
              const date = moment.utc((this.value));
              return `${date.format("DD/MM/YY")}`;
            },
          },
        },
        yAxis: {
          startOnTick: false,
          endOnTick: false,
          title: {
            text: 'Amount'
          },
          labels: {
            format: '{value}$'
          },
          maxPadding: 0.2,
        },
        tooltip: {
          useHTML: true,
          headerFormat: '<table>',
          pointFormat: '<tr><th colspan="2"><h3>{point.name}</h3></th></tr>' +
            '<tr><th>Amount:</th><td>{point.amount}</td></tr>' +
            '<tr><th>Status:</th><td>{point.status}</td></tr>' +
            '<tr><th>Description:</th><td>{point.description}</td></tr>',
          footerFormat: '</table>',
          followPointer: true
        },
        plotOptions: {
          series: {
            dataLabels: {
              enabled: true,
              format: '{point.amount}'
            }
          }
        },
        series: [{
          data: transactions.map(transaction => ({ x: moment(transaction.value_date).valueOf(), y: transaction.amount, z: transaction.amount, name: transaction.category, status: transaction.status, description: transaction.description, amount: transaction.amount, color: stringToColour(transaction.category ?? '') })),
          colorByPoint: true
        }],
      })
  }, [transactions])

  const stringToColour = (str) => {
    let hash = 0;
    str.split('').forEach(char => {
      hash = char.charCodeAt(0) + ((hash << 5) - hash)
    })
    let colour = '#'
    for (let i = 0; i < 3; i++) {
      const value = (hash >> (i * 8)) & 0xff
      colour += value.toString(16).padStart(2, '0')
    }
    return colour
  }

  //Histogram
  useEffect(() => {
    setOptions2({
      title: {
        text: 'Histogram',
        align: 'left'
      },
      subtitle: {
        text: 'Transactions',
        align: 'left'
      },
      yAxis: [{
        title: { text: 'Cantidad' }
      }, {
        title: { text: 'Histogram' },
        opposite: true
      }],
      xAxis: [{
        title: { text: 'Cantidad' },
        alignTicks: false
      }, {
        title: { text: 'Histogram' },
        alignTicks: false,
        opposite: true
      }],
      plotOptions: {
        histogram: {
          accessibility: {
            point: {
              valueDescriptionFormat: '{index}. {point.x:.3f} to {point.x2:.3f}, {point.y}.'
            }
          }
        }
      },
      series: [{
        name: 'Histogram',
        type: 'histogram',
        xAxis: 1,
        yAxis: 1,
        baseSeries: 's1',
        zIndex: -1
      }, {
        name: 'Cantidad',
        type: 'scatter',
        data: transactions.map(transaction => transaction.amount),
        id: 's1',
        marker: {
          radius: 1.5
        }
      }],
      responsive: {
        rules: [{
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }]
      }
    })
  }, [transactions])

 // CHART
  // useEffect(() => {
  //   setOptions3({
  //     title: {
  //       text: "Por período de tiempo"
  //     },
  //     subtitle: {},
  //     xAxis: {},
  //     yAxis: {},
  //     legend: {},
  //     plotOptions: {},
  //     series: [{}],
  //     responsive: {}
  //   }),
  //     setOptions4({
  //       title: {
  //         text: "Valores descriptivos"
  //       },
  //       subtitle: {},
  //       xAxis: {},
  //       yAxis: {},
  //       legend: {},
  //       plotOptions: {},
  //       series: [{}],
  //       responsive: {}
  //     }),
  //     setOptions5({
  //       title: {
  //         text: "Gráficas temporales"
  //       },
  //       subtitle: {},
  //       xAxis: {},
  //       yAxis: {},
  //       legend: {},
  //       plotOptions: {},
  //       series: [{}],
  //       responsive: {}
  //     })
  // }, []),

  return (
    <>
      <main className={styles.main}>
        <div className={styles.center} xs={12}>
          <Table rows={transactions} isLoading={isLoading} isError={isError} account={account} />
        </div>
        <div className={styles.center}>
          <Grid container spacing={2} marginTop={3} marginBottom={6} paddingX={1.5}>
            <Grid item xs={12} md={7}>
              <Card sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignContent: 'center', height: '50vh', borderRadius: '16px', padding: 4, backgroundColor: '#f0f0f0' }}>
                {
                  isLoading ? (
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '90vh' }}>
                      <CircularProgress />
                    </Box>
                  ) : (
                    options !== null && <Chart options={options} />
                  )
                }
              </Card>
            </Grid>
            <Grid item xs={12} md={5}>
              <Card sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignContent: 'center', height: '50vh', borderRadius: '16px', padding: 4, backgroundColor: '#f0f0f0' }}>
                {
                  isLoading ? (
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '90vh' }}>
                      <CircularProgress />
                    </Box>
                  ) : (
                    options2 !== null && <Chart options={options2} />
                  )
                }
              </Card>
            </Grid>
            {/* <Grid item xs={5}>
              <Card sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignContent: 'center', height: '50vh', borderRadius: '16px', padding: 4, backgroundColor: '#f0f0f0' }}>
                {
                  isLoading ? (
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '90vh' }}>
                      <CircularProgress />
                    </Box>
                  ) : (
                    options3 !== null && <Chart options={options3} />
                  )
                }
              </Card>
            </Grid>
            <Grid item xs={7}>
              <Card sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignContent: 'center', height: '50vh', borderRadius: '16px', padding: 4, backgroundColor: '#f0f0f0' }}>
                {
                  isLoading ? (
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '90vh' }}>
                      <CircularProgress />
                    </Box>
                  ) : (
                    options4 !== null && <Chart options={options4} />
                  )
                }
              </Card>

            </Grid>
            <Grid item xs={12}>
              <Card sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignContent: 'center', height: '50vh', borderRadius: '16px', padding: 4, backgroundColor: '#f0f0f0' }}>
                {
                  isLoading ? (
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '90vh' }}>
                      <CircularProgress />
                    </Box>
                  ) : (
                    options5 !== null && <Chart options={options5} />
                  )
                }
              </Card>
            </Grid> */}
          </Grid>
        </div>
      </main>
    </>
  );
}
