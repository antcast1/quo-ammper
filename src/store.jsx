import { configureStore } from '@reduxjs/toolkit';
import transactionsSlice from './redux/reducers/transactionsReducer'; 
import accountSlice from './redux/reducers/accountReducer'; 

export const store = configureStore({ 
    reducer: {
        transactionsSlice, 
        accountSlice
    }  
});
